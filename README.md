# Automatic Somatosensory System (2021)




## Description

Archived automatic somatosensory system project from 2013.


## Getting Started

### Dependencies

* C Standard Library, GCC
* Linux


### Usage

* Connect sensor & PCI Board
* Compile data2img.cpp
* Run executable


## Author

Austin Richie

https://gitlab.com/ar39907/
