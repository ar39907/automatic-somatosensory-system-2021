
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main( int argc, char* argv[] )
{

	if (argc != 2) {
		fprintf(stderr,"must provide an output file base\n");
	}

	int raw[25][25];
	int scaled[25][25];

	int fileno = 1;

	while (cin) {

		for(int i=0; i<25; i++) for(int j=0; j<25; j++) cin >> raw[i][j];
//		for(int i=0; i<25; i++) for(int j=0; j<25; j++) raw[i][j] = i*j;

		int imin = 1000;
		int imax = -1000;
		for(int i=0; i<25; i++) for(int j=0; j<25; j++) {
			if (raw[i][j] < imin) imin = raw[i][j];
			if (raw[i][j] > imax) imax = raw[i][j];
		}

		float s = (-imin > imax)? -imax : imax;

		if (s==0) continue; // s=255;

		for(int i=0; i<25; i++) for(int j=0; j<25; j++) {
			scaled[i][j] = (int)((float)raw[i][j] * 255/s);
		}
	
		string filename = string(argv[1]) + "-" + to_string(fileno) + ".ppm";

		cout << filename << " " << s << endl;

		ofstream ofs(filename);
		ofs << "P3" << endl;
		ofs << "250 250" << endl;
		ofs << "255" << endl;

		for(int i=0; i<25; i++) {
			for(int k = 0; k<10; k++) {
				for(int j=0; j<25; j++) {
					int x = scaled[i][j];
					for(int l=0; l<10; l++) {
						if (x < 0) {
							ofs << -x << " 0 0 ";
						} else {
							ofs << "0 " << x << " 0 ";
						}
					}
				}
				ofs << endl;
			}
		}

		ofs.close();

		fileno++;
	}

}

